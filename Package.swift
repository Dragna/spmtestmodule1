// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SPMTestModule1",
    platforms: [.iOS(.v12)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "SPMTestModule1",
            targets: ["SPMTestModule1"]),
        .library(
            name: "SPMTestModule1UI",
            targets: ["SPMTestModule1UI"])
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://github.com/ReactiveX/RxSwift.git", .upToNextMajor(from: "5.1.1")),
        .package(url: "https://github.com/SwiftyBeaver/SwiftyBeaver.git", .upToNextMajor(from: "1.8.0")),
        .package(name: "Reachability", url: "https://github.com/ashleymills/Reachability.swift", .upToNextMajor(from: "5.1.0")),
        .package(url: "https://github.com/Swinject/Swinject", .upToNextMajor(from: "2.7.1"))
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "SPMTestModule1",
            dependencies: [.product(name: "RxSwift", package: "RxSwift"),
                           .product(name: "RxCocoa", package: "RxSwift"),
                           .product(name: "RxRelay", package: "RxSwift"),
                           "SwiftyBeaver",
                           "Swinject",
                           "Reachability"],
            path: "Sources/SPMTestModule1"),
        .target(
            name: "SPMTestModule1UI",
            dependencies: ["SPMTestModule1"],
            path: "Sources/SPMTestModule1UI")
    ]
)
